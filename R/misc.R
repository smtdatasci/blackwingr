### --- ### --- ### --- ### --- ### --- ###
###   -----  HELPER FUNCTIONS   -----   ###
### --- ### --- ### --- ### --- ### --- ###



#' Function converts R dataframe to D3 compatible json object
#' @export
data_to_json <- function(data) {
  jsonlite::toJSON(data, dataframe = "rows", auto_unbox = FALSE, rownames = TRUE)
}


#' This function converts parameter estimates from the logit scale (i.e. coef(glm_fit) into the probability scale.
#' @param logit Numeric.
#' @export
scale_logit_to_prob <- function(logit){
  odds <- exp(logit)
  prob <- odds / (1 + odds)
  return(prob)
}

#' Convert Meters per second to miles per hour
#' @export
kmh_to_mph <- function(kmh){
  return(kmh/1.609)
}

#' Collect the unique value in a column
#' @param column DataFrame column.
#' @export
get_unique_value <- function (column)
{
  unique_val <- unique(column)
  res <- NA
  if (length(unique_val) == 1) {
    res <- unique_val
  }
  return(unique_val)
}



#' Load an EDGE parquet object
#' @export
loadEDGEobj <- function(objName='pbp',
                        event_name="M_S_Mens_Singles",
                        year=2021,
                        tournament = 'wimbledon',
                        is_playerNames = FALSE,
                        root_path = NULL,
                        is_input_csv=FALSE){

  if(is.null(root_path)){
    data_repo <-  paste0('./data_parquet/',year,'/',tournament,'/')
  } else{
    data_repo <- root_path
  }

  if(!is_input_csv){
    if(is_playerNames){
      FileName <- paste0(data_repo, objName, '.parquet')
    } else{
      FileName <- paste0(data_repo,  event_name, '/', objName, '.parquet')
    }
    # -- Load object data Piece
    DF <- arrow::read_parquet(FileName)
    DF <- DF  %>% mutate_all(na_if,"")
  } else{
    if(is_playerNames){
      dir_path <- paste0(root_path)
    } else{
      dir_path <- paste0(root_path,event_name,'/')
    }
    # -- Load object data Piece
    DF <- load_csv(objName = objName,
                   root_path = dir_path)
    DF <- DF  %>% mutate_all(na_if,"")
  }



  return(DF)

}



#' Load an EDGE shiny object
#' @export
loadEDGEshinyobj <- function(params, objName){
  params <- c(params, objName)

  DF <- do.call(loadEDGEobj, c(params))

  return(DF)

}




#' Get vector of group by variable names
#' @param sets_df DataFrame with all raw serve counts by set, match and event.
#' @param grouping_var string of desired outer grouping level
#' @export
get_group_by_column_names <- function(grouping_variable){
  if(grouping_variable == 'By Match'){
    my_cols <- c("matchID", "Round", "Date", 'PlayerName', "Opponent")
    #my_cols <- c( "Round", "Date", 'PlayerName', "Opponent")

  } else if(grouping_variable == 'By Set'){
    my_cols <- c("matchID", "Round", "Date", 'setNum','PlayerName', "Opponent")
    #my_cols <- c("Round", "Date", 'setNum','PlayerName', "Opponent")

  } else if(grouping_variable == 'By Tournament'){
    my_cols <- c('last_match_date','PlayerName', "num_matches")

  }

  return(my_cols)
}


