% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/plot_dumbbell_pressure_points.R
\name{d3_plot_dumbbell_return}
\alias{d3_plot_dumbbell_return}
\title{Dumbbell plot comparing regular point to pressure point performance.}
\usage{
d3_plot_dumbbell_return(
  all_DF,
  pressure_name = "Break Points",
  stat_name = "Return Pts Won",
  player_names = c("Novak Djokovic", "Rafael Nadal", "Roger Federer",
    "Alexander Zverev")
)
}
\description{
Dumbbell plot comparing regular point to pressure point performance.
}
