# -- Test out changes to blackwingR
library(blackwingR)
library(dplyr)
library(lubridate)
library(xml2)

### --- ### --- ### --- ### --- ### --- ###
### --- Testing Scrapers ----
### --- ### --- ### --- ### --- ### --- ###
scrape_challenges(filename="ausopen.xml",year=2016)
scrape_matches_played(filename="ausopen.xml",year=2016)
scrape_player_info(filename="ausopen.xml",year=2016)
scrape_umpire_info(filename="ausopen.xml",year=2016)
scrape_truth(filename="ausopen.xml",year=2016)
scrape_PBP_data(filename="ausopen.xml",year=2016)

### --- ### --- ### --- ### --- ### --- ###
# -- Testing Data processing ----
### --- ### --- ### --- ### --- ### --- ###
#data_path= "./data_parquet/2016/ausopen/FS/raw/"
data_path= "./data_parquet/2020/frenchopen/MS/raw/"


rawPBP <- load_parquet("pbp",data_path)
# -- Matches played
rawMatches <- load_parquet('matches',data_path)
# -- Player Roster
rawRoster <- load_parquet("roster",data_path)
# -- Umpire Roster
rawUmpire <- load_parquet("umpireroster",data_path)

rawTruth <- load_parquet("truth",data_path)

# -- Challenge Data
rawChallenges <- tryCatch({
  DF <- load_parquet('challenges',data_path)
  DF <- DF %>%
    mutate(challengeTime = lubridate::hms(Time),
           is_challenge = TRUE,
           challengeDate = Date,
           challengeID = paste(matchID,SetNum, GameNum, PointNum, sep='_')) %>%
    select(challengeID, challengeTime, is_challenge, challengeDate) %>% distinct
  DF},
# -- If challenges DF does not exist, then return empty dataframe.
  error=function(cond){
    message(paste("Challenge data not found."))
    DF = data.frame(
      challengeID=NA,
      challengeTime=NA,
      is_challenge=NA,
      challengeDate=NA)
    DF})


PBPDF <- load_EDGEfullpbp(rawPBP=rawPBP,rawMatches=rawMatches,rawRoster=rawRoster,rawChallenges=rawChallenges)
View(PBPDF %>% filter(matchID =="MS210"))




# -- Test data loads from S3 ----
library(SMRT)
library(dplyr)
library(arrow)
library(blackwingR)

aws_tennis_credentials <- read.table("aws_tennis_credentials.txt",header = TRUE)
year <- 2022
tournament <- "wimbledon"
is_mens=TRUE
matchID="MS701"


# -- SMT scoring data
# Test data load
params <-list(
  year=2022,
  tournament="wimbledon",
  is_mens=TRUE,
  obj = "shinyPBP.parquet"
)
PBPDF <- read_tennis_s3_obj(payload=params, credentials=aws_tennis_credentials)




ball_payload <- list(
  year=year,
  tournament=tournament,
  is_mens=is_mens,
  obj="BallTracks",
  matchID=matchID
)
ball_tracks <- read_tennis_s3_hawkeye(ball_payload, aws_tennis_credentials)

player_payload <- list(
  year=year,
  tournament=tournament,
  is_mens=is_mens,
  obj="PlayerTracks",
  matchID=matchID
)
player_tracks <- read_tennis_s3_hawkeye(player_payload, aws_tennis_credentials)

ballImpacts_payload <- list(
  year=year,
  tournament=tournament,
  is_mens=is_mens,
  obj="BallImpacts",
  matchID=matchID
)
ballImpacts<- read_tennis_s3_hawkeye(ballImpacts_payload, aws_tennis_credentials)

PBPDF_payload <- list(
  year=year,
  tournament=tournament,
  is_mens=is_mens,
  obj="hawkeyePBPDF",
  matchID=matchID
)
PBPDF <- read_tennis_s3_hawkeye(PBPDF_payload, aws_tennis_credentials)

rallyShots_payload <- list(
  year=year,
  tournament=tournament,
  is_mens=is_mens,
  obj="RallyShots",
  matchID=matchID
)
rallyShots <- read_tennis_s3_hawkeye(rallyShots_payload, aws_tennis_credentials)

# -- Live data feed import ----
src_path <-  "C:/TennisGHO/AnalyticsDashboardDataFeed/Output/"
matches <- loadEDGEobj(objName = 'matches',root_path = "C:/TennisGHO/AnalyticsDashboardDataFeed/Output/",
                       event_name = 'MS',
                       is_input_csv =TRUE)



