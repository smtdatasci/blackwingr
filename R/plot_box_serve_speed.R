#' Function creates DataFrame for plotting boxplots of serve speed across serve number and serve direction.
#' @param PBPDF Full Play-by-play dataframe object.
#' @export
#'
process_speed_data <- function(PBPDF){


  speed_data <- PBPDF %>%
    mutate(isUnreturnedServe =  ( (serveResult %in% c('Ace','Winner')) | (returnQuality %in% c('Forced Error', 'Unforced Error')) ) ) %>%
    select(matchID, serveNum, ServerName,
                                 ReturnerName, serveDir, serveResult,
                                 serveKMH, serveMPH, Score, game_score, set_score,
                                 isSvrPtWinner, isBrkPt, isGamePt, isAd,isUnreturnedServe) %>%
    #filter(!is.na(serveKMH)) %>%
    #filter(serveKMH > 0) %>%
    filter(serveDir != 'Net Fault') %>%
    mutate(hover_label = paste0(matchID, '\n', 'Opponent: ', ReturnerName, '\n',
                                'Speed: ', serveMPH, '\n',
                                'Result: ', serveResult, '\n',
                                'Set Score: ', set_score, '\n',
                                'Game Score: ', game_score, '\n',
                                'Point Score: ', Score))

  speed_data$serveDir <- factor(speed_data$serveDir,
                                levels = c('Centre', 'Body', 'Wide'))

  speed_data$serveNum <- ifelse(speed_data$serveNum ==1, '1st Serve', '2nd Serve')

  return(speed_data)

}

#' Process PBP for serve speed boxplots
#' @export
save_boxplot_speed_data <- function(PBPDF, save_root){

  serve_speed <- process_speed_data(PBPDF = PBPDF)

  plot_root <- paste0(save_root,'plot/')
  create_directory(plot_root)

  speedfilename <- paste0(plot_root, 'serve_speed.parquet')

  arrow::write_parquet(x = serve_speed,
                       #compression = 'uncompressed',
                       sink = speedfilename)

  print('Saved serve speed data.')

}

#' Function plots serve speed using D3
#' @export
plot_serve_speed_boxplot <- function(speed_data, my_player,
                                     court_side = 'Both',
                                     serve_type = "In play",
                                     highlight_score='None'){


  if(my_player ==''){
    return(NULL)
  } else if(my_player == 'Tournament'){
    player_data <- speed_data
  } else {
    player_data <- speed_data %>%
      filter(ServerName == my_player)

  }

  player_data$serveDir <- as.character(player_data$serveDir)
  player_data$serveDir <- ifelse(player_data$serveDir == 'Centre','T', player_data$serveDir)
  player_data$serveMPH <- ifelse(player_data$serveMPH==0,NA,player_data$serveMPH)

  court_side <- ifelse(court_side== "Both", 'Both Court Sides',ifelse(court_side=='Advantage', 'Advantage Court Serves','Deuce Court Serves'))

  tourn_avg_data <- speed_data

  if(court_side == 'Deuce Court Serves'){
    player_data <- player_data %>%
      filter(!isAd)
    tourn_avg_data <- speed_data %>%
      filter(!isAd)
  } else if (court_side == 'Advantage Court Serves'){
    player_data <- player_data %>%
      filter(isAd)
    tourn_avg_data <- speed_data %>%
      filter(isAd)
  }


  if(serve_type == "In play"){
    player_data <- player_data %>%
      filter(serveResult %in% c("Ace","In play","Winner"))
    tourn_avg_data <- tourn_avg_data %>%
      filter(serveResult %in% c("Ace","In play","Winner"))
  } else if(serve_type == "Fault"){
    player_data <- player_data %>%
      filter(serveResult %in% c("Fault","Foot Fault"))
    tourn_avg_data <- tourn_avg_data %>%
      filter(serveResult %in% c("Fault","Foot Fault"))
  } else if(serve_type == "Unreturned Serve"){
    player_data <- player_data %>% filter(isUnreturnedServe)
    tourn_avg_data <- tourn_avg_data %>% filter(isUnreturnedServe)
  } else {
    player_data <- player_data %>%
      filter(serveResult %in% c("Ace"))
    tourn_avg_data <- tourn_avg_data %>%
      filter(serveResult %in% c("Ace"))
  }

  tourn_avg_data <- tourn_avg_data %>% group_by(serveNum) %>% summarise(avg_speed = median(serveMPH,na.rm=TRUE))
  summary_data <- player_data %>%
    filter(!is.na(serveKMH)) %>%
    #filter(serveKMH > 0) %>%
    group_by(serveDir, serveNum) %>%
    summarise(median = median(serveMPH, na.rm = TRUE),
              q1 = quantile(serveMPH, 0.25, na.rm=TRUE),
              q3 = quantile(serveMPH, 0.75, na.rm = TRUE),
              max_speed = max(serveMPH, na.rm = TRUE),
              min_speed = min(serveMPH, na.rm = TRUE),
              #serves_in_play = sum(serveResult %in% c('Ace','In play','Winner')),
              #faults = sum(serveResult == 'Fault'),
              #srv_pts_won = sum(isSvrPtWinner, na.rm=TRUE),
              n_obs=n()) %>%
    rowwise() %>%
    mutate(
      iqr = q3-q1,
      box_max=min(max_speed, 1.5*iqr + q3),
      box_min=max(min_speed, q1- 1.5*iqr),
      #perc_in_play = case_when(
      #  n_obs > 0 ~ paste0(round(100*serves_in_play/n_obs,1), '%'),
      #  TRUE ~ NA_character_)
    ) %>% ungroup %>%
    left_join(
      player_data %>% group_by(serveNum) %>%
        summarise(tot = n()),
      by='serveNum'
    ) %>% ungroup %>%
    mutate(
      choice_freq = paste0(round(100*n_obs/tot,1), '%'),
      #pts_won = case_when(
      #  serveNum == '1st Serve' ~ paste0(round(100*srv_pts_won/serves_in_play),'%'),
      #  serveNum == '2nd Serve' ~ paste0(round(100*srv_pts_won/(serves_in_play+faults)),'%'),
      #  TRUE ~ NA_character_)
    ) %>%
    as.data.frame()

  y_min <- 0 #80
  y_max <- 150 #250

  data_list <- list('summary_data'=summary_data,
                    'raw_data' = player_data %>%
                      filter(!is.na(serveMPH)) %>%
                      filter(serveMPH > 0))


  r2d3(data=data_to_json(data_list),
       script = "./plots/speed_boxplot.js",
       d3_version = 6,
       options = list(player_name = my_player,
                      y_min=y_min, y_max=y_max,
                      subtitle=court_side,
                      FS_avg = tourn_avg_data[1,2][[1]],
                      SS_avg = tourn_avg_data[2,2][[1]],
                      hovery = 100,
                      hoverx=50,
                      score=highlight_score),
       width = 800,
       height = 450)
}


#' Function creates an interactive boxplot (on ggiraph) of serve speed across serve number and serve direction for a single player using ggiraph.
#' @param complete_speed_data Processed Play-by-play dataframe object (see process_speed_data() function).
#' @param player_name Name of server of interest.
#' @export
#'
#'
create_servespeed_boxplot <- function(complete_speed_data, player_name = 'Novak Djokovic',...){

  speed_data <- complete_speed_data %>%
    filter(ServerName == player_name)

  tooltip_css <- "background-color:#ffffff;border-radius:5px;padding:5px;color:black;font-family:Gotham Medium;font-size:12pt;box-shadow:5px 5px 5px rgba(0,0,0,0.2);"

  speed_summary <- speed_data %>% filter(serveNum=='1st Serve') %>% pull(serveKMH) %>% summary()
  q1 <- speed_summary[2]
  q3 <- speed_summary[5]


  speed_data$isFast2nd = ( (speed_data$serveNum== '2nd Serve') & (speed_data$serveKMH > (q1)) )

  anno <- data.frame(xstar = c(2.5, 2.5),
                     ystar = c(q3-0.05, q3-7),
                     lab = c("", "***Speeds consistent with\n1st serves***"),
                     serveNum = c("1st Serve", "2nd Serve"))

  set.seed(824)
  speed_boxplot <- ggplot(data = speed_data,
                          aes(y=serveKMH, x = serveDir)) +
    annotate("rect", xmin = -Inf, xmax = Inf, ymin=q1+0.5, ymax=q3,
             fill ='indianred',
             alpha = 0.25) +
    geom_hline(yintercept = q1+0.5, color='indianred', linetype='dashed') +
    geom_hline(yintercept = q3, color='indianred', linetype='dashed') +
    stat_boxplot(geom ='errorbar', width = 0.25) +

    # -- Add all serve speed points that are NOT fast 2nd serves...
    geom_jitter(data = speed_data %>% filter(!isFast2nd),
                aes(fill = as.factor(serveNum)),
                show.legend = FALSE,
                size = 1.5,
                width = 0.2,
                alpha =0.5, shape =21) +
    geom_boxplot_interactive()
    # geom_boxplot_interactive(aes(fill=as.factor(serveNum)),
    #              show.legend = FALSE,
    #              outlier.shape = NA,
    #              alpha=0.75) #+
    # geom_text(data = anno,
    #           aes(x=xstar, y=ystar, label = lab),
    #           size = 3,
    #           fontface='bold',
    #           family = 'GOTHAM-MEDIUM',
    #           colour = "darkred") +
    # # -- Add fast second serve speeds
    # geom_point_interactive(data = speed_data %>% filter(isFast2nd),
    #                        aes(tooltip = hover_label),
    #                        fill = '#FFDC14',
    #                        size = 2,
    #                        alpha =0.8, shape =21,
    #                        position = position_jitter(w = 0.38, h = 0)) +
    # scale_fill_manual(values = c("indianred", '#202691'),
    #                   labels = c('1st Serve', '2nd Serve')) +
    # facet_wrap(~serveNum) +
    # #ylim(c(98, 231), breaks =c(100,140,180,220) ) +
    # coord_cartesian(ylim=c(98, 231)) +
    # scale_y_continuous( breaks=c(100,140,180,220)) +
    # labs(title =paste0(player_name, ' Tournament Serve Speed Profile'),
    #      y = 'Serve Speed (KMH)',
    #      x = "") +
    # default_theme(
    #   strip_col = 'white',
    #   inner_bg_col = 'white',
    #   outer_bg_col = 'white',
    #   title_size = 14#,
    #   #...
    #   )

  girafe(width_svg = 7, height_svg = 4.5,
         ggobj = speed_boxplot,
         options = list(opts_tooltip(css = tooltip_css)))
}



