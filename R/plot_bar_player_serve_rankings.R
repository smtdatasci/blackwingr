# -- Functions to
# --> 1) Create datasets for player rankings
# --> 2) Plot player rankings with D3

#' Function creates an interactive D3 barplot of player serve performance
#' summaries and their percentile ranks
#' @export
create_player_serve_rank_plot <- function(serveDisplay_tournament,
                                          my_player='Andy Murray'
                                          ){

  # my_params = list(event_name = 'M_S_Mens_Singles',
  #                  year = 2021,
  #                  tournament='wimbledon',
  #                  is_playerNames = FALSE,
  #                  root_path = NULL)
  #serveDisplay_tournament <-  loadEDGEshinyobj(params = my_params, objName='serve/serve_tournament')

  if(! 'PlayerName' %in% colnames(serveDisplay_tournament)){
    columnID_change <- match(colnames(serveDisplay_tournament), c('ServerName', 'ReturnerName', 'Player'))
    columnID_change <- which(!is.na(columnID_change))
    colnames(serveDisplay_tournament)[columnID_change] <- 'PlayerName'
  }

  plot_data <- serveDisplay_tournament %>%
    filter(PlayerName != 'Tournament') %>%
    select(PlayerName, ServePtsWonPerc, FSPtsPerc, FSIn, SSPtsPerc, SSIn) %>%
    mutate(SPts_Rank = round(percent_rank(ServePtsWonPerc),4),
           FSPts_Rank = round(percent_rank(FSPtsPerc),4),
           FSIn_Rank = round(percent_rank(FSIn),4),
           SSPts_Rank = round(percent_rank(SSPtsPerc),4),
           SSIn_Rank = round(percent_rank(SSIn),4)
    ) %>%
    arrange(desc(FSPtsPerc))

  # -- Add tournament medians
  # -- Note: Median taken across players
  tournament_data <- serveDisplay_tournament %>%
    filter(PlayerName != 'Tournament') %>%
    select(ServePtsWonPerc, FSPtsPerc, FSIn, SSPtsPerc, SSIn)

  tournament_medians <- apply(tournament_data,2,median,na.rm=TRUE)

  #plot_data <- bind_rows(plot_data, c('Tournament', tournament_medians, rep(0.5,5)))
  plot_data <- plot_data %>% add_row(
    PlayerName = 'Tournament',
    ServePtsWonPerc = tournament_medians['ServePtsWonPerc'],
    FSPtsPerc = tournament_medians['FSPtsPerc'],
    FSIn = tournament_medians['FSIn'],
    SSPtsPerc = tournament_medians['SSPtsPerc'],
    SSIn = tournament_medians['SSIn'],
    SPts_Rank = 0.5,
    FSPts_Rank = 0.5,
    FSIn_Rank = 0.5,
    SSPts_Rank = 0.5,
    SSIn_Rank = 0.5)

  player_data <- plot_data %>%
    filter(PlayerName==my_player)

  player_plot_data <- data.frame(
    stat = c('Overall Serve Pts Won', '1st Serve Pts Won', '1st Serves In', '2nd Serve Pts Won', '2nd Serves In'),
    value = unlist(player_data[1,2:6]),
    perc_rank =  unlist(player_data[1,7:11])
  )

  player_plot_data <-
    player_plot_data %>%
    mutate(label = paste0(round(value,2)*100, '%'))


  d3_bar <- r2d3(data=player_plot_data, script = "./plots/bar_player_serve_card.js",
                 d3_version = 6,
                 options = list(player_name = my_player,
                                hovery = 100),
                 width = 800,
                 height = 450)

  d3_bar


}

# --- || --- || --- || --- #
# ----- SERVE TEMPO ----- #
# --- || --- || --- || --- #
#' Get all relevant service tempo stats for plot.
#' @export
get_serve_tempo_DF <- function(PBPDF, grouping_var = 'By Tournament'){
  avg_game_length <- get_average_serve_game_length(PBPDF = PBPDF,
                                                   grouping_var=grouping_var)
  match_tempo <-
    cbind_serve_clock(serveResults=avg_game_length, PBPDF=PBPDF,
                      grouping_var=grouping_var) %>% ungroup()


  # -- Add rally duration
  avg_rally_duration <- get_average_rally_duration(PBPDF=PBPDF, grouping_var = 'By Tournament')

  # -- Add Efficiency stats
  serve_efficiency <- summarise_serve_stats(PBPDF=PBPDF,grouping_cols='ServerName') %>%
    mutate(
      unreturned_serves = percent(UnreturnedServes/(srv_in_play+serve_winners_and_aces)),
      srv_games_held = percent(SrvGmsHeld/ SrvGms),
      shutout_games = percent(GmsHeldatLove/SrvGms)
    ) %>% select(ServerName,unreturned_serves,srv_games_held,shutout_games)

  match_tempo <- match_tempo %>%
    left_join(avg_rally_duration %>% select(ServerName, median_rally_duration),
              by='ServerName') %>%
    left_join(serve_efficiency,by='ServerName')

  # -- Add Percentile and rankings
  # -- Note: # Here, Low = Fast; High = Slow ....i.e. don't use desc()' in rank()
  #            dense_rank() does not skip rankings when there ties, while rank() does skip the rankings
  #           Ex: dense_rank(): 1,1,2,2,3 vs rank(): 1,1,3,3,5

  match_tempo <-
    match_tempo %>%
    mutate(mean_ppsg_prank = round(percent_rank(mean_ppsg),2),
           mean_ppsg_rank = rank(mean_ppsg),
           # med_ppsg_prank = round(percent_rank(med_ppsg),2),
           # med_ppsg_rank = rank(med_ppsg),

           # mean_sec_psg_prank = round(percent_rank(mean_sec_psg),2),
           # mean_sec_psg_rank = rank(mean_sec_psg),
           median_sec_psg_prank = round(percent_rank(median_sec_psg),2),
           median_sec_psg_rank = rank(median_sec_psg),

           avgServeClock_prank = round(percent_rank(avgServeClock),2),
           avgServeClock_rank = rank(avgServeClock),
           median_time_bn_serves_prank = round(percent_rank(median_time_bn_serves),2),
           median_time_bn_serves_rank = rank(median_time_bn_serves),

           # mean_2nd_serve_time_prank = round(percent_rank(mean_2nd_serve_time),2),
           # mean_2nd_serve_time_rank = rank(mean_2nd_serve_time)
           median_time_bn_serves = round(median_time_bn_serves,2),
           median_2nd_serve_time_prank = round(percent_rank(median_2nd_serve_time),2),
           median_2nd_serve_time_rank = rank(median_2nd_serve_time),

           median_rally_duration_prank = round(percent_rank(median_rally_duration),2),
           median_rally_duration_rank = rank(median_rally_duration),

           # Efficiency stats
           unreturned_serves_prank = round(percent_rank(unreturned_serves),2),
           unreturned_serves_rank = rank(unreturned_serves),

           srv_games_held_prank = round(percent_rank(srv_games_held),2),
           srv_games_held_rank = rank(srv_games_held),

           shutout_games_prank = round(percent_rank(shutout_games),2),
           shutout_games_rank = rank(shutout_games)





    ) %>%
    select(ServerName,s_games,
           mean_ppsg,mean_ppsg_prank,mean_ppsg_rank,
           #mean_sec_psg, mean_sec_psg_prank, mean_sec_psg_rank,
           median_sec_psg, median_sec_psg_prank, median_sec_psg_rank,
           #avgServeClock, avgServeClock_prank, avgServeClock_rank,
           median_time_bn_serves, median_time_bn_serves_prank, median_time_bn_serves_rank,
           #mean_2nd_serve_time, mean_2nd_serve_time_prank, mean_2nd_serve_time_rank
           median_2nd_serve_time, median_2nd_serve_time_prank, median_2nd_serve_time_rank,
           median_rally_duration, median_rally_duration_prank, median_rally_duration_rank,
           unreturned_serves, unreturned_serves_prank, unreturned_serves_rank,
           srv_games_held, srv_games_held_prank, srv_games_held_rank,
           shutout_games, shutout_games_prank, shutout_games_rank
           )

  return(match_tempo)
}

#' @export
save_serve_tempo_DF <- function(PBPDF, save_root){

  serve_tempo_DF <- get_serve_tempo_DF(PBPDF=PBPDF)

  plot_root <- paste0(save_root,'plot/')
  create_directory(plot_root)

  serve_tempo_filename <- paste0(plot_root, 'serve_tempo.parquet')

  arrow::write_parquet(x = serve_tempo_DF,
                       #compression = 'uncompressed',
                       sink = serve_tempo_filename)

  print('Saved Serve Tempo D3 Data')
}

#' Function creates an interactive D3 barplot of player serve tempo and
#' their percentile ranks
#' @export
create_player_serve_tempo_rank_plot <- function(serve_tempo_DF,
                                                my_player='Andy Murray'){

  # my_params = list(event_name = 'M_S_Mens_Singles',
  #                  year = 2021,
  #                  tournament='wimbledon',
  #                  is_playerNames = FALSE,
  #                  root_path = NULL)
  # serve_tempo_DF <-  loadEDGEshinyobj(params = my_params, objName='plot/serve_tempo')


  # -- Hover Tools: Min/Max/Median/Rank
  # tournament_stats <- serve_tempo_DF %>%
  #   filter(ServerName != 'Tournament') %>%
  #   select(median_sec_psg, #avgServeClock,
  #          median_time_bn_serves,
  #          median_2nd_serve_time,median_rally_duration, mean_ppsg )
  #
  # # -- Extract Minimum, Median, Mean, Max
  # tournament_summary <- as.data.frame(apply(tournament_stats,2,summary)) %>% t()
  # colnames(tournament_summary) <- c('min','q1','median','mean','q3','max')
  #
  # tournament_summary[c(2:4),] <- round(tournament_summary[c(2:4),], 1)
  # tournament_summary[1,] <- paste0(tournament_summary[1,]%/%60, ' min ', round(tournament_summary[1,]%%60,0), ' sec')

  tournament_medians <- apply(serve_tempo_DF[,-1],2,median,na.rm=TRUE)

  #plot_data <- bind_rows(plot_data, c('Tournament', tournament_medians, rep(0.5,5)))
  plot_data <- serve_tempo_DF %>% add_row(
    ServerName = 'Tournament',
    median_sec_psg = tournament_medians['median_sec_psg'],
    mean_ppsg = tournament_medians['mean_ppsg'],
    median_rally_duration = tournament_medians['median_rally_duration'],
    median_time_bn_serves = tournament_medians['median_time_bn_serves'],
    median_2nd_serve_time = tournament_medians['median_2nd_serve_time'],
    median_sec_psg_prank = 0.5,
    median_time_bn_serves_prank = 0.5,
    median_2nd_serve_time_prank = 0.5,
    median_rally_duration_prank = 0.5,
    mean_ppsg_prank = 0.5)

  player_data <- plot_data %>%
    filter(ServerName==my_player) %>%
    mutate(median_sec_psg_str=paste0(median_sec_psg%/%60, ' min ', round(median_sec_psg%%60,0), ' sec'),
           mean_ppsg = round(mean_ppsg,2), median_rally_duration = round(median_rally_duration,2)
    )

  player_plot_data <- data.frame(
    stat = c('Service Game Length', 'Time between Points', 'Time to 2nd Serve', 'Rally Duration'),
    value = unlist(player_data[1,c('median_sec_psg_str','median_time_bn_serves','median_2nd_serve_time','median_rally_duration')]),
    perc_rank =  unlist(player_data[1,c('median_sec_psg_prank','median_time_bn_serves_prank','median_2nd_serve_time_prank','median_rally_duration_prank')]),
    numeric_rank = unlist(player_data[1,c('median_sec_psg_rank','median_time_bn_serves_rank','median_2nd_serve_time_rank','median_rally_duration_rank')])
  )

  player_plot_data <- player_plot_data %>%
    mutate(d3_prank= 1-(perc_rank))

  # Adjust value for plot label aesthetics
  player_plot_data[2,'value'] <- paste0(player_plot_data[2,'value'], ' sec')
  player_plot_data[3,'value'] <- paste0(player_plot_data[3,'value'], ' sec')
  player_plot_data[4,'value'] <- paste0(player_plot_data[4,'value'], ' sec')


  #player_plot_data <- cbind(player_plot_data,tournament_summary)

  serve_tempo_plot <- r2d3(data=player_plot_data, script = "./plots/serve_tempo.js",
                           d3_version = 6,
                           options = list(player_name=my_player, hovery = 100),
                           width = 900,
                           height = 400)

  return(serve_tempo_plot)


}


#' Function creates an interactive D3 barplot of player serve efficiency and
#' their percentile ranks
#' @export
create_player_serve_efficiency_rank_plot <- function(serve_tempo_DF,my_player='Andy Murray'){

  tournament_medians <- apply(serve_tempo_DF[,-1],2,median,na.rm=TRUE)

  plot_data <- serve_tempo_DF %>% add_row(
    ServerName = 'Tournament',
    srv_games_held = tournament_medians["srv_games_held"],
    shutout_games = tournament_medians["shutout_games"],
    unreturned_serves = tournament_medians["unreturned_serves"],
    mean_ppsg = tournament_medians["mean_ppsg" ],
    srv_games_held_prank = 0.5,
    shutout_games_prank = 0.5,
    unreturned_serves_prank = 0.5,
    mean_ppsg_prank = 0.5)

  player_data <- plot_data %>%
    filter(ServerName==my_player) %>%
    mutate(mean_ppsg = round(mean_ppsg,2))


  player_plot_data <- data.frame(
    stat = c('Serve Games Held', 'Love Games', 'Unreturned Serves','Points Played per Game'),
    value = unlist(player_data[1,c('srv_games_held','shutout_games','unreturned_serves','mean_ppsg')]),
    perc_rank =  unlist(player_data[1,c('srv_games_held_prank','shutout_games_prank','unreturned_serves_prank','mean_ppsg_prank')]),
    numeric_rank = unlist(player_data[1,c('srv_games_held_rank','shutout_games_rank','unreturned_serves_rank','mean_ppsg_rank')]))

  player_plot_data[4,"perc_rank"] <- 1 - player_plot_data[4,"perc_rank"]

  player_plot_data <- player_plot_data %>%
    mutate(d3_prank=(perc_rank))

  # Adjust value for plot label aesthetics
  player_plot_data[1,'value_lab'] <- paste0(100*player_plot_data[1,'value'],  '% of games')
  player_plot_data[2,'value_lab'] <- paste0(100*player_plot_data[2,'value'], '% of games')
  player_plot_data[3,'value_lab'] <- paste0(100*player_plot_data[3,'value'], '% of serves')
  player_plot_data[4,'value_lab'] <- paste0(player_plot_data[4,'value'], ' points')



  #player_plot_data <- cbind(player_plot_data,tournament_summary)

  serve_tempo_plot <- r2d3(data=player_plot_data, script = "./plots/serve_efficiency.js",
                           d3_version = 6,
                           options = list(player_name=my_player, hovery = 100),
                           width = 900,
                           height = 400)

  return(serve_tempo_plot)}




#' Function creates an interactive D3 barplot of player return performance
#'  summaries and their percentile ranks
#' @export
create_player_return_rank_plot <- function(returnDisplay_tournament,
                                           my_player='Andy Murray'
                                           ){

  # my_params = list(event_name = 'M_S_Mens_Singles',
  #                  year = 2021,
  #                  tournament='wimbledon',
  #                  is_playerNames = FALSE,
  #                  root_path = NULL)
  # returnDisplay_tournament <-  loadEDGEshinyobj(params = my_params, objName='return/return_tournament')

  plot_data <- returnDisplay_tournament %>%
    filter(PlayerName != 'Tournament') %>%
    select(PlayerName, perc_rpw, perc_rpw_1st, perc_RIP1, perc_rpw_2nd, perc_RIP2,
           RtnPts,RtnPtsWon, FSRtnPts,FSRtnPtsWon, SSRtnPts,SSRtnPtsWon) %>%
    mutate(perc_rpw_prank = round(percent_rank(perc_rpw),4),
           perc_rpw_1st_prank = round(percent_rank(perc_rpw_1st),4),
           perc_RIP1_prank = round(percent_rank(perc_RIP1),4),
           perc_rpw_2nd_prank = round(percent_rank(perc_rpw_2nd),4),
           perc_RIP2_prank = round(percent_rank(perc_RIP2),4),

           perc_rpw_rank = round(rank(desc(perc_rpw)),0),
           perc_rpw_1st_rank = round(rank(desc(perc_rpw_1st)),0),
           perc_RIP1_rank = round(rank(desc(perc_RIP1)),0),
           perc_rpw_2nd_rank = round(rank(desc(perc_rpw_2nd)),0),
           perc_RIP2_rank = round(rank(desc(perc_RIP2)),0)
    )

  #plot_data %>% arrange(perc_rpw) %>% View()

  tournament_medians <- apply(plot_data[,-1],2,median, na.rm=TRUE)

  #plot_data <- bind_rows(plot_data, c('Tournament', tournament_medians, rep(0.5,5)))
  plot_data <- plot_data %>% add_row(
    PlayerName = 'Tournament',
    perc_rpw = tournament_medians["perc_rpw"],
    perc_rpw_1st = tournament_medians["perc_rpw_1st"],
    perc_RIP1 = tournament_medians["perc_RIP1"],
    perc_rpw_2nd = tournament_medians["perc_rpw_2nd"],
    perc_RIP2 = tournament_medians["perc_RIP2"],
    perc_rpw_prank = 0.5,
    perc_rpw_1st_prank = 0.5,
    perc_RIP1_prank = 0.5,
    perc_rpw_2nd_prank = 0.5,
    perc_RIP2_prank = 0.5)



  player_data <- plot_data %>%
    filter(PlayerName==my_player)

  player_plot_data <- data.frame(
    stat = c('Overall Return Pts Won', '1st Returns Won', '1st Returns In Play', '2nd Returns Won', '2nd Returns In Play'),
    value = unlist(player_data[1,c("perc_rpw","perc_rpw_1st","perc_RIP1","perc_rpw_2nd","perc_RIP2")]),
    perc_rank =  unlist(player_data[1,c("perc_rpw_prank","perc_rpw_1st_prank","perc_RIP1_prank","perc_rpw_2nd_prank","perc_RIP2_prank")]),
    rank = unlist(player_data[1,c("perc_rpw_rank","perc_rpw_1st_rank","perc_RIP1_rank","perc_rpw_2nd_rank","perc_RIP2_rank")]),
    hover_lab = paste0( c('Return points played: ','1st serve return points: ','1st serve return points: ','2nd serve return points: ','2nd serve return points: '),
                        unlist(player_data[1,c("RtnPts","FSRtnPts","FSRtnPts","SSRtnPts","SSRtnPts")]) )
  )



  player_plot_data <-
    player_plot_data %>%
    mutate(value = paste0(round(value,2)*100, '%'))


  d3_bar <- r2d3(data=player_plot_data, script = "./plots/bar_player_return_card.js",
                 d3_version = 6,
                 options = list(player_name = my_player,
                                hovery = 100),
                 width = 800,
                 height = 450)

  d3_bar


}



### Pressure points player card:
# serveDisplay_tournament <-
#   load_parquet(objName = "serve_tournament",
#                root_path="./data_parquet/2022/wimbledon/MS/processed/serve/")
# returnDisplay_tournament <-
#   load_parquet(objName = "return_tournament",
#                root_path="./data_parquet/2022/wimbledon/MS/processed/return/")
# rallyDisplay_tournament <-
#   load_parquet(objName = "rally_tournament",
#                root_path="./data_parquet/2022/wimbledon/MS/processed/rally/")
#
# colnames(rallyDisplay_tournament)
#
# "perc_setup_pts_won", "srvSetUpPtsPlayed", "srvSetUpPtsWon"
#
#
# serve <- select(serveDisplay_tournament, ServerName,BrkPtSavedPerc)
# return <- select(returnDisplay_tournament, PlayerName, perc_bp, perc_game_breaks)
# ovr <- select(rallyDisplay_tournament,PlayerName, perc_setup_pts_won)
#
# fullData <- serve %>%
#   left_join(return, by=c("ServerName"="PlayerName")) %>%
#   left_join(ovr, by=c("ServerName"="PlayerName"))




