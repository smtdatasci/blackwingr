# -- Functions to Convert raw TennisGHO .xml files into tidy dataframes. -----
# -- Then, save DataFrames into `.csv` files ----

#library(xml2)
#library(dplyr)

# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
#' Function extracts play-by-play portion of xml data, and saves datatable.
#' @param TennisScoringXML XML file from tennis scoring DOM.
#' @param year Tournament year (integer)
#' @param isSinglesOnly Boolean
#' @export
scrape_PBP_data <- function(TennisScoringXML,tournament, year=2021, isSinglesOnly = TRUE){
  # -- Args:
  # -- * filename: [str] of .xml file.
  # -- * data_path: [str] directory path containing xml files.

  # -- Purpose:
  # -- Given a Filename, load the .xml data and create the
  # -- pointbypoint DataFrame for an entire tournament event.

  xmlEvents <- xml_find_all(x = TennisScoringXML, xpath = '/Tournament/Events/Event')

  if(isSinglesOnly){
    xmlEvents <- filter_single_events(xmlEvents=xmlEvents)
  }

  # -- For each event in the Tournament, extract the point-by-point data into a DataFrame and save
  # -- into a `.csv` file.

  lapply(xmlEvents, function(xmlcurEvent){

    # || --- || --- || --- || --- ||  --- || --- ||  --- || --- ||
    #  -- Save Event Attributes ----
    # || --- || --- || --- || --- ||  --- || --- ||  --- || --- ||

    # -- get Event DF.
    getDF <- get_event_dataframe(xmlcurEvent)

    # -- Save DF.
    saveFileRoot <- generateFileRoot(xmlcurEvent, year=year, tournament=tournament)
    arrow::write_parquet(x =getDF, sink = paste0(saveFileRoot,"pbp.parquet"))
    gc()

  })


}

# Test:
#save_PBP_data(filename = "Wimbledon2018.xml")


# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
#' @export
get_event_dataframe <- function(xmlcurEvent){
  # -- Args: xmlcurEvent: xml Object containing all pointbypoint trail data for an Event.

  # -- Purpose:
  # -- Given a list of matches in a tournament event,
  # -- return a DataFrame of the pointbypoint trail.

  # xmlcurEvent <- xmlEvents[[1]]

  # -- Get List of Matches from event data.
  xmlMatches <- xml_find_all(x = xmlcurEvent, xpath = 'Round/Match')

  # -- Look at just first 5 matches for prototyping.
  #xmlMatches <- xmlMatches[1:5]

  # -- For each match, get list of point trail.
  eventList <- lapply(xmlMatches, extract_match_datalist)
  eventList <- unlist(eventList, recursive = FALSE)

  # -- Convert List to a DataFrame.
  eventPBPDF <- dplyr::bind_rows(eventList)


  eventPBPDF <- eventPBPDF %>%
    mutate(setNum = as.integer(setNum),
           gameNum = as.integer(gameNum),
           pointNum = as.integer(pointNum),
           serveNum = as.integer(serveNum)) %>%
    arrange(matchID, setNum, gameNum, pointNum, serveNum, serveTime) %>%
    mutate(ServerID = gsub("([0-9]+).*$", "\\1", pointSrv),
           ReturnerID = gsub("([0-9]+).*$", "\\1", pointRcv)) %>%
    select(matchID, matchAltID, setNum, gameNum, pointNum, serveNum, ServerID, ReturnerID, everything())

  # -- Ensure requisite columns exist
  required_columns <- c(matchStartDate=NA_real_, matchStartTime=NA_real_)
  if( length( setdiff(names(required_columns), colnames(eventPBPDF)) ) != 0 ){
    eventPBPDF <- tibble::add_column(eventPBPDF, !!!required_columns[setdiff(names(required_columns), colnames(eventPBPDF))])
  }


  return(eventPBPDF)
}

# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
#' @export
extract_match_datalist <- function(xmlcurMatch, alt=TRUE){
  # -- Args:
  # -- xmlcurMatch: xml Object of a Match.
  # -- alt: BOOL specifying which version save point trail function to use.

  # -- Purpose:
  # -- Given Match data, return pointbypoint trail in a List.

  # xmlcurMatch <- xmlMatches[[1]]
  curMatchAttrs <- xmlcurMatch %>% xml_attrs()

  names(curMatchAttrs) <- paste0("match", names(curMatchAttrs))

  # -- Get all Sets played in the match.
  xmlSets <- xml_find_all(x = xmlcurMatch,
                          xpath = 'Points/Set')

  # -- For each Set, save the point-by-point trail into a List.
  matchList <- lapply(xmlSets, extract_set_datalist, alt)
  matchList <- unlist(matchList, recursive = FALSE)

  # -- Add Match Attributes to each item of the matchlist ----
  matchListwithAttrs <- lapply(matchList, function(point){
    return(c(point, curMatchAttrs))
  })

  print(paste0('Done: ', curMatchAttrs['matchID']))

  return(matchListwithAttrs)
}


# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
#' @export
extract_set_datalist <- function(xmlcurSet, alt=TRUE){
  # -- Args:
  # -- xmlcurSet: xml Object of a Set.
  # -- alt: BOOL specifying which version save point trail function to use.

  # -- Purpose:
  # -- Given Set data, return pointbypoint trail in a List.

  # xmlcurSet <- xmlSets[[1]]
  curSetAttrs <- xml_attrs(xmlcurSet)

  names(curSetAttrs) <- paste0("set", names(curSetAttrs))

  # -- Get all games played in the current set.
  xmlGames <- xml_find_all(x = xmlcurSet,
                           xpath = 'Game')

  # -- For each Game within the set, save the point-by-point trail into a List.
  setList <- lapply(xmlGames, extract_game_datalist, curSetAttrs, alt)

  setList <- unlist(setList, recursive = FALSE)

  return(setList)

}

# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
#' @export
extract_game_datalist <- function(xmlcurGame, curSetAttrs, alt=TRUE){
  # -- Args:
  # -- xmlcurGame: xml Object of a Game.
  # -- curSetAttrs: Vector of Set attributes to add to each game element
  # -- alt: BOOL

  # -- Purpose:
  # -- Function extracts all points within a game into a list.

  # xmlcurGame <- xmlGames[[12]]
  # -- Get current game attributes -----
  curGameAttrs <- xml_attrs(xmlcurGame)
  names(curGameAttrs) <- paste0("game", names(curGameAttrs))

  # -- Get list of all points played in the current game.
  xmlPoints <- xml_find_all(x = xmlcurGame, xpath = 'Point')
  #xmlPoints <- xmlPoints[1:2]
  pointList <- if (!alt) {
    lapply(xmlPoints, extract_point_datalist, curGameAttrs)
  } else {
    lapply(xmlPoints, extract_point_datalist_2, curGameAttrs)
  }

  ## pointList <- lapply(xmlPoints, extract_point_datalist, curGameAttrs)
  ## pointList2 <- lapply(xmlPoints, extract_point_datalist_2, curGameAttrs)

  gameList <- unlist(pointList, recursive = FALSE)

  # Add set attributes to all point items in this Game List
  gameList <- lapply(gameList, function(point){
    return(c(point, curSetAttrs))
  })

  return(gameList)

}

# -- Test out the extract_game_datalist() function
#extract_game_datalist(xmlcurGame)

# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
# --- || --- ||--- || --- || --- || --- || --- || --- || --- || --- ||
#' @export
extract_point_datalist_2 <- function (xmlcurPoint, curGameAttrs) {
  # -- Args:
  # -- xmlcurPoint: xml Object of a Point.
  # -- curGameAttrs: Vector of Game attributes to add to each Point element.

  # -- Purpose:
  # -- Return pointlist trail in a List.


  # xmlcurPoint <- xmlPoints[[3]]
  xmlcur <- as.character(xmlcurPoint)
  Point <- regmatches (xmlcur, gregexpr("<Point .*?>", xmlcur))
  Serve <- regmatches (xmlcur, gregexpr("<Serve .*?>", xmlcur))
  Return <- regmatches (xmlcur, gregexpr("<Return .*?>", xmlcur))
  End <- regmatches (xmlcur, gregexpr("<End .*?>", xmlcur))

  Props <- function (str, name="") {
    str <- str %>% gsub ("<[^ ]*", "", .) %>% gsub(">$", " ", .)
    obj1 <- regmatches (str, gregexpr(' [^ ]+?=".*?"', str)) %>% unlist
    objkeys <- gsub (" +([^ ]+?)=.*", "\\1", obj1)
    objvalues <- gsub (' +([^ ]+?)="(.*)"', "\\2", obj1)
    names (objvalues) <- paste0(name, objkeys)
    return (objvalues)
  }

  PointProps <- if (length(Point[[1]]) > 0) Props (Point, "point") else {out <- list(NA); names(out[[1]]) = "point"; out[[1]]}
  PointReturn <- if (length(Return[[1]]) > 0) Props (Return[[1]], "return") else {out <- list(NA); names(out[[1]]) = "return"; out[[1]]}
  PointEnd <- if (length(End[[1]]) > 0) Props (End[[1]], "end") else {out <- list(NA); names(out[[1]]) = "end"; out[[1]]}

  if(length(Serve[[1]]) == 0){

    Serve1 <- list(NA); names(Serve1[[1]]) = "serve"
    pointList <- list( c( PointProps, Serve1[[1]], PointReturn, PointEnd, curGameAttrs ) )

  } else if (length(Serve[[1]]) > 1) {
    Serve1 <- Props (Serve[[1]][1], "serve")
    Serve2 <- Props (Serve[[1]][2], "serve")

    firstservePointVec <- c ( PointProps, Serve1, PointReturn, PointEnd, curGameAttrs )
    firstservePointVec[c('pointWinner', 'pointTm1FH', 'pointTm1BH', 'pointTm2FH', 'pointTm2BH', 'pointRally')] <- NA
    secondservePointVec <- c ( PointProps, Serve2, PointReturn, PointEnd, curGameAttrs )

    pointList <- list(firstservePointVec, secondservePointVec)

  } else {
    Serve1 <- Props (Serve[[1]][1], "serve")
    pointList <- list( c( PointProps, Serve1, PointReturn, PointEnd, curGameAttrs ) )
  }

  return (pointList)

}


#' @export
extract_point_datalist <- function(xmlcurPoint,
                                   curGameAttrs){
  # xmlcurPoint is a list, possibly containing the following sublists:
  # Attributes
  # Serve (1 or 2)
  # Return
  # End

  # xmlcurPoint <- xmlPoints[[3]]


  curListPointAttrs <- xml_attrs(xmlcurPoint)

  returnList <- xml_find_all(x = xmlcurPoint,
                             xpath = 'Return') %>% xml_attrs()

  endList <- xml_find_all(x = xmlcurPoint,
                          xpath = 'End') %>% xml_attrs()


  if(length(endList) == 0){
    endList <- list(NA)
  }

  if(length(returnList) == 0){
    returnList <- list(NA)
  }

  # -- Change names of point attributes, serve-return and end-point.
  names(curListPointAttrs) <- paste0("point", names(curListPointAttrs))
  names(returnList[[1]]) <- paste0("return", names(returnList[[1]]))
  names(endList[[1]]) <- paste0("end", names(endList[[1]]))


  # -- Get Serve Attributes
  serveList <- xml_find_all(x = xmlcurPoint, xpath = 'Serve') %>% xml_attrs()

  numServes <- length(serveList)

  if(numServes == 1){

    names(serveList[[1]]) <- paste0("serve", names(serveList[[1]]))

    pointList <- list( c( curListPointAttrs, serveList[[1]], returnList[[1]], endList[[1]], curGameAttrs ) )

  } else if(numServes == 2){
    # -- If the point had 2 serves, then will need to add an extra row.
    firstserve <- serveList[[1]]
    secondserve <- serveList[[2]]
    names(firstserve) <- paste0("serve", names(firstserve))
    names(secondserve) <- paste0("serve", names(secondserve))

    firstservePointVec <- c ( curListPointAttrs, firstserve, returnList[[1]], endList[[1]], curGameAttrs )
    firstservePointVec[c('pointWinner', 'pointTm1FH', 'pointTm1BH', 'pointTm2FH', 'pointTm2BH', 'pointRally')] <- NA
    secondservePointVec <- c ( curListPointAttrs, secondserve, returnList[[1]], endList[[1]], curGameAttrs )
    pointList <- list(firstservePointVec, secondservePointVec)
  }

  return(pointList)
}


# -- Test out the extract_point_datalist() function
# curGameAttrs <- c("1", "false", "false", "1" )
# names(curGameAttrs) <- c('gameNum', 'gameIsPenalty', 'gameIsTB', 'gameWinner' )
#
# extract_point_datalist(xmlPoints[[1]], curGameAttrs)
# extract_point_datalist(xmlPoints[[4]], curGameAttrs)













