# --- ### --- ### --- ### --- ### --- ### --- ### --- ### --- ###
# --- ### --- ### --- ### --- ### --- ### --- ### --- ### --- ###
# --- QA Script to test accuracy of Data Science Algorithms ----
# --- ### --- ### --- ### --- ### --- ### --- ### --- ### --- ###
# --- ### --- ### --- ### --- ### --- ### --- ### --- ### --- ###
library(blackwingR)
library(dplyr)

# -- EDGE LOAD PBPDF ----
setwd("C:/Users/ptea/OneDrive - Sportsmedia Technology Corporation/smt_data_science/tennis/tennis-EDGE/")
#setwd("C:/Users/ptea/OneDrive - Sportsmedia Technology Corporation/blackwingR")

year <- 2021
event_name <- 'M_S_Mens_Singles'
#event_name <- 'F_S_Womens_Singles'
tournament <- 'wimbledon'
#tournament <- 'frenchopen'
PBPDF <- load_EDGEfullpbp(year=year, tournament=tournament, event_name=event_name)


# What matches are missing long rallies?
PBPDF %>% filter(!is.na(PointWinner)) %>%
  group_by(matchID) %>%
  summarise(max_rally = max(pointRally, na.rm=TRUE)) %>%
  filter(max_rally <= 4) %>% pull (matchID)


# Which time between points are problematic?
PBPDF %>%
  group_by(matchID) %>%
  summarise(missing = sum(is.na(serveclock))) %>%
  arrange(desc(missing)) %>% View()

# MS406 (FAA vs Zverev) has a lot of missing serve clock obs
MS406 <- PBPDF %>% filter(matchID == 'MS406') %>%
  mutate(#user_time_bn_pts = case_when(is_changeover_pt ~ 'Changeover. Input not required.',TRUE ~''),
         user_time_bn_pts = case_when(setNum==1 & gameNum==1 & pointNum==1 & serveNum==1 ~ '0',
                                      TRUE ~''),
         user_time_bn_pts_comment = case_when(is_changeover_pt ~ '',
                                              time_bn_serves > 34 ~'What happened?',
                                              time_bn_serves < 5 ~ 'What happened?',
                                              TRUE ~ ''),
         user_rally_duration = ''
  ) %>%
  select(setNum,gameNum,pointNum,serveNum,ServerName,ReturnerName,
         Score,
         #time_bn_serves, serveclock,rallyduration,
         user_time_bn_pts,user_time_bn_pts_comment, user_rally_duration)

clipr::write_clip(MS406)

# MS403 (Khachanov vs Korda); 5th set advantage set
MS403 <- PBPDF %>% filter(matchID == 'MS403') %>%
  mutate(user_time_bn_pts = case_when(setNum==1 & gameNum==1 & pointNum==1 & serveNum==1 ~ '0',
                                      TRUE ~''),
         user_time_bn_pts_comment = case_when(is_changeover_pt ~ '',
                                              time_bn_serves > 34 ~'What happened?',
                                              time_bn_serves < 5 ~ 'What happened?',
                                              TRUE ~ ''),
         user_rally_duration = ''
  ) %>%
  select(setNum,gameNum,pointNum,serveNum,ServerName,ReturnerName,
         Score,
         #time_bn_serves, serveclock,rallyduration,
         user_time_bn_pts,user_time_bn_pts_comment, user_rally_duration)

clipr::write_clip(MS403)

# MS303 Schwartzmann vs Fucsovics
MS303 <- PBPDF %>% filter(matchID == 'MS303') %>%
  mutate(user_time_bn_pts = case_when(setNum==1 & gameNum==1 & pointNum==1 & serveNum==1 ~ '0',
                                      TRUE ~''),
         user_time_bn_pts_comment = case_when(is_changeover_pt ~ '',
                                              time_bn_serves > 34 ~'What happened?',
                                              time_bn_serves < 5 ~ 'What happened?',
                                              TRUE ~ ''),
         user_rally_duration = ''
  ) %>%
  select(setNum,gameNum,pointNum,serveNum,ServerName,ReturnerName,
         Score,
         #time_bn_serves, serveclock,rallyduration,
         user_time_bn_pts,user_time_bn_pts_comment, user_rally_duration)

clipr::write_clip(MS303)



