% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/plot_dumbbell_pressure_points.R
\name{create_breakpoint_on_return_DF}
\alias{create_breakpoint_on_return_DF}
\title{Function creates DataFrame for return performance on break points}
\usage{
create_breakpoint_on_return_DF(PBPDF)
}
\arguments{
\item{PBPDF}{Full Play-by-play dataframe object.}
}
\description{
Function creates DataFrame for return performance on break points
}
