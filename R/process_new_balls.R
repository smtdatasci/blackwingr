add_new_balls_column <- function(PBPDF){

  matchOrder <- PBPDF %>% group_by(matchID,setNum,gameNum) %>% select(matchID,setNum,gameNum) %>% distinct()
  cum_game_match <- matchOrder %>% group_by(matchID) %>% mutate(cumGames = row_number())

  is_New_Ball <- cum_game_match %>% mutate(isNewBall = cumGames %in% c(8,17,26,35,44,53,62) )
  is_New_Ball <- is_New_Ball %>% select(matchID,setNum,gameNum,isNewBall)
  newPBPDF <- PBPDF %>% left_join(is_New_Ball, by = c('matchID','setNum','gameNum'))
  return(newPBPDF)
}

#' @export
#'
New_ball_stats <- function(PBPDF){

  PBPDF <- add_new_balls_column(PBPDF=PBPDF)

  serve_stats_avg <- PBPDF %>%
    summarise(mean_svr_speed_mph_all =  mean(serveMPH, na.rm = TRUE),
              median_svr_mph_all = median(serveMPH, na.rm =TRUE),
              mean_svr_kmh_all = mean(serveKMH, na.rm=TRUE),
              median_svr_kmh_all = median(serveKMH, na.rm= TRUE),
              mean_svr_point_won_all = mean(isSvrPtWinner, na.rm = TRUE),
              Aces_All = mean(serveResult =="Ace", na.rm=T))

  serve_new_ball <- PBPDF %>% filter(isNewBall ==TRUE) %>%
    summarise(mean_svr_speed_mph_new =  mean(serveMPH, na.rm = TRUE),
              median_svr_mph_new = median(serveMPH, na.rm =TRUE),
              mean_svr_kmh_new = mean(serveKMH, na.rm=TRUE),
              median_svr_kmh_new = median(serveKMH, na.rm= TRUE),
              mean_svr_point_won_new = mean(isSvrPtWinner, na.rm = TRUE),
              Aces_New = mean(serveResult =="Ace", na.rm=T))

  serve_old_ball <- PBPDF %>% filter(isNewBall ==FALSE) %>%
    summarise(mean_svr_speed_mph_old =  mean(serveMPH, na.rm = TRUE),
              median_svr_mph_old = median(serveMPH, na.rm =TRUE),
              mean_svr_kmh_old = mean(serveKMH, na.rm=TRUE),
              median_svr_kmh_old = median(serveKMH, na.rm= TRUE),
              mean_svr_point_won_old = mean(isSvrPtWinner, na.rm = TRUE),
              Aces_Old = mean(serveResult =="Ace",na.rm=T))

  serve_stats_full <- bind_cols(serve_stats_avg,serve_new_ball,serve_old_ball)

  return(serve_stats_full)
}

#New_ball_stats(PBPDF)
