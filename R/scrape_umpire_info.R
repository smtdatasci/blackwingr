#' Function scrapes Umpire Info Dataframe.
#' @param TennisScoringXML XML file from tennis scoring DOM.
#' @param year Tournament year (integer)
#' @export
scrape_umpire_info <- function(TennisScoringXML, tournament, year){

  xmlEvents <- xml_find_all(x = TennisScoringXML, xpath = '/Tournament/Events/Event')

  # -- Player Information ----
  # | PlayerID | Player Name | DOB | ... |
  UmpireInfo <- xml_find_all(x=TennisScoringXML, xpath='.//Umpires') %>%
    xml_find_all( xpath='.//Umpire')

  # -- Obtain list of vectors. Each vector contains information pertinent to one player.
  UmpireInfo <- xml_attrs(UmpireInfo)

  UmpireInfoDF <- dplyr::bind_rows(UmpireInfo)

  UmpireInfoDF$ShortName = paste0( substr(UmpireInfoDF$FirstName,1,1), '.', UmpireInfoDF$LastName)
  UmpireInfoDF$FullName = stringr::str_to_title(paste(UmpireInfoDF$FirstName, UmpireInfoDF$LastName))


  # -- Save DF.
  mens_target_repo <- paste('./data_parquet',year, tournament,"MS",sep='/')
  womens_target_repo <- paste('./data_parquet',year, tournament,"FS",sep='/')
  create_directory(mens_target_repo)
  create_directory(womens_target_repo)

  mens_nested_target_repo <- paste0(mens_target_repo,'/','raw/')
  womens_nested_target_repo <- paste0(womens_target_repo,'/','raw/')
  create_directory(mens_nested_target_repo)
  create_directory(womens_nested_target_repo)

  arrow::write_parquet(x = UmpireInfoDF, sink = paste0(mens_nested_target_repo,"umpireroster.parquet"))
  arrow::write_parquet(x = UmpireInfoDF, sink = paste0(womens_nested_target_repo,"umpireroster.parquet"))

  #return(UmpireInfoDF)

}
