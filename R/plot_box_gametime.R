# -- ALL RELEVANT FUNCTIONS TO CREATE GAMETIME DATAFRAME AND PLOT


#' Save elapsed Game time boxplot data
#' @export
save_gametime_data <- function(PBPDF, save_root){
  game_lengths <- calculate_serve_game_length(PBPDF=PBPDF)

  plot_root <- paste0(save_root,'plot/')
  create_directory(plot_root)

  gametime_filename <- paste0(plot_root, 'gametime.parquet')

  arrow::write_parquet(x = game_lengths,
                       #compression = 'uncompressed',
                       sink = gametime_filename)

  print('Saved Game Time D3 Data')

}

#' Function creates DataFrame for plotting boxplots of elapsed gametime.
#' @param game_lengths Dataframe object.
#' @export

create_game_length_boxplot <- function(game_lengths, my_player){

  player_data <- game_lengths %>%
    filter(ServerName == my_player ) %>%
    mutate(game_result = ifelse(isSvrGmWinner, 'Service Games Won', 'Service Games Lost'))

  player_data <- game_lengths %>%
    filter(ServerName == my_player | ReturnerName == my_player) %>%
    mutate(name = my_player,
           opponent = case_when(
             ServerName == my_player ~ ReturnerName,
             ReturnerName == my_player ~ ServerName,
             TRUE ~ NA_character_),
           position = case_when(
             ServerName == my_player ~ 'Serving',
             ReturnerName == my_player ~ 'Returning',
             TRUE ~ NA_character_),
           game_result = case_when(
             position == 'Serving' & isSvrGmWinner ~ 'Games Won',
             position == 'Serving' & !isSvrGmWinner ~ 'Games Lost',

             position == 'Returning' & isSvrGmWinner ~ 'Games Lost',
             position == 'Returning' & !isSvrGmWinner ~ 'Games Won',
             TRUE ~ NA_character_)) %>%
    select(name, opponent, position, game_result, elapsed_game_time, matchID, setNum, gameNum)



  summary_data <- player_data %>%
    group_by(position,game_result) %>%
    summarise(median = median(elapsed_game_time, na.rm = TRUE),
              q1 = quantile(elapsed_game_time, 0.25, na.rm=TRUE),
              q3 = quantile(elapsed_game_time, 0.75, na.rm = TRUE),
              max_gametime = max(elapsed_game_time, na.rm = TRUE),
              min_gametime = min(elapsed_game_time, na.rm = TRUE),
              n_obs=n()) %>%
    rowwise() %>%
    mutate(
      iqr = q3-q1,
      box_max=min(max_gametime, 1.5*iqr + q3),
      box_min=max(min_gametime, q1- 1.5*iqr)
    ) %>% ungroup %>%
    as.data.frame()


  y_min <- 0
  y_max <- 12*60

  data_list <- list('summary_data'=summary_data,
                    'raw_data' = player_data %>%
                      filter(!is.na(elapsed_game_time)))

  r2d3(data=data_to_json(data_list),
       script = "./plots/gametime_boxplot.js",
       d3_version = 6,
       options = list(player_name = my_player,
                      y_min=y_min, y_max=y_max,
                      hovery = 100,
                      hoverx=30),
       width = 800,
       height = 450)


}
