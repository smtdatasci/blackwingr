% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/plot_bar_player_serve_rankings.R
\name{get_serve_tempo_DF}
\alias{get_serve_tempo_DF}
\title{Get all relevant service tempo stats for plot.}
\usage{
get_serve_tempo_DF(PBPDF, grouping_var = "By Tournament")
}
\description{
Get all relevant service tempo stats for plot.
}
