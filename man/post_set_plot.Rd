% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/plot_post_match_summary.R
\name{post_set_plot}
\alias{post_set_plot}
\title{Function creates an interactive D3 barplot of points won broken down by winners and errors across sets played}
\usage{
post_set_plot(plot_data, select_matchID)
}
\description{
Function creates an interactive D3 barplot of points won broken down by winners and errors across sets played
}
