% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/save_display_data.R
\name{save_data_PBP}
\alias{save_data_PBP}
\title{Save Play-by-play data. This function essentially makes all categorical variables more readable for the general audience.}
\usage{
save_data_PBP(PBPDF, save_root)
}
\arguments{
\item{PBPDF}{Processed play-by-play data.}

\item{save_root}{Path to root saving directory.}
}
\description{
Save Play-by-play data. This function essentially makes all categorical variables more readable for the general audience.
}
