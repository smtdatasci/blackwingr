#' Save all cleaned and processed data that feeds into EDGE shiny app.
#' @param src_root Root path to where raw input data exists.
#' @param target_root Root path to where output data should be saved.
#' @export
process_shiny_data <- function(src_root = "./data_parquet/2016/ausopen/FS/raw/",
                               target_root ="./data_parquet/2016/ausopen/FS/processed/",
                               tournament_begin_date = "2016-02-01"){


  # --- ## --- ## --- ## --- ## --- ## --- ## --- ## --- ## --- ##
  # -- Load in necessary raw Data Pieces ----
  # --- ## --- ## --- ## --- ## --- ## --- ## --- ## --- ## --- ##
  # -- Player Roster
  Roster <- load_parquet("roster", src_root)

  # Remove duplicate rows
  # Remove "test" IDs...
  Roster <- dplyr::distinct(Roster)
  if("IsTest" %in% colnames(Roster)){
    Roster <- dplyr::filter(Roster, IsTest != "true")
  }
  # Remove players without a Team ID
  Roster <- dplyr::filter(Roster,!is.na(teamID))


  # -- Umpire Names
  Umpires <- load_parquet("umpireroster", src_root)

  # -- Matches played
  match_data <- load_parquet("matches", src_root)

  # -- Challenge data
  # -- Challenge Data
  challenges <- tryCatch({
    DF <- load_parquet('challenges',data_path)
    DF <- DF %>%
      mutate(challengeTime = lubridate::hms(Time),
             is_challenge = TRUE,
             challengeDate = Date,
             challengeID = paste(matchID,SetNum, GameNum, PointNum, sep='_')) %>%
      select(challengeID, challengeTime, is_challenge, challengeDate) %>% distinct
    DF},
    # -- If challenges DF does not exist, then return empty dataframe.
    error=function(cond){
      message(paste("Challenge data not found."))
      DF = data.frame(
        challengeID=NA,
        challengeTime=NA,
        is_challenge=NA,
        challengeDate=NA)
      DF})


  # -- Play-by-play Full Play-by-play Data
  rawPBP <- load_parquet("pbp", src_root)

  fullPBP <- load_EDGEfullpbp(rawPBP=rawPBP,match_data,Roster,challenges)
  # --- ## --- ## --- ## --- ## --- ## --- ## --- ## --- ## --- ##
  # -- End Load in necessary Data Pieces -
  # --- ## --- ## --- ## --- ## --- ## --- ## --- ## --- ## --- ##


  # -- Figure out where to save the data...
  create_directory(target_root)

  # -- Save Player Bios -----
  save_data_player_bio(rawRoster=Roster, save_root=target_root, tournament_begin_date = tournament_begin_date)

  # -- Save Match Overview and Match Tempo Data -----
  save_data_match_catalogue(PBPDF=fullPBP, rawMatches = match_data, rawRoster = Roster, rawUmpire = Umpires, save_root=target_root)

  # -- Save Play-by-play data: -----
  save_data_PBP(PBPDF=fullPBP, save_root=target_root)

  # -- Save Serve Display Data: -----
  save_data_serve(PBPDF=fullPBP,rawMatches=match_data,rawRoster=Roster,rawUmpire=Umpires,save_root = target_root)

  # -- Save Return Display Data: -----
  save_data_return(PBPDF=fullPBP,rawMatches=match_data,rawRoster=Roster,rawUmpire=Umpires,save_root = target_root)

  # -- Save Rally Display Data: -----
  save_data_rally(PBPDF=fullPBP,rawMatches=match_data,rawRoster=Roster,rawUmpire=Umpires,save_root = target_root)

  # -- Save Truth Display Data: -----
  # save_data_truth(params=params,match_data=match_data, PlayerIDDF=PlayerIDDF,
  #                 PlayerNamesDF=PlayerNamesDF, save_root=target_path_event,
  #                 isEDGE=TRUE)


  # -- Save plot data: ----
  save_boxplot_speed_data(PBPDF=fullPBP, save_root=target_root)
  save_breakpoint_dumbbell_data(PBPDF=fullPBP, save_root=target_root)
  save_closing_set_dumbbell_data(PBPDF=fullPBP, save_root=target_root)
  save_TB_dumbbell_data(PBPDF=fullPBP, save_root=target_root)
  save_match_summary_bar_data(PBPDF=fullPBP, save_root=target_root)
  save_serve_tempo_DF(PBPDF=fullPBP, save_root=target_root)
  save_gametime_data(PBPDF=fullPBP, save_root=target_root)
  save_cumulative_pts_won_data(PBPDF=fullPBP, save_root=target_root)
  save_shots_in_play_timeseries(PBPDF=fullPBP, save_root=target_root)
  save_gametime_timeseries(PBPDF=fullPBP, save_root=target_root)

  # ****************
  save_donut_match_time_DF(PBPDF=fullPBP,rawMatches=match_data,rawRoster=Roster,rawUmpire=Umpires,save_root = target_root)
  save_return_dumbbell_data(PBPDF=fullPBP, save_root=target_root)

  message('Done data processsing!')

}
